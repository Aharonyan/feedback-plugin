<?php

/**
 * Gutenberg block to display Feedback List.
 *
 * @package FeedBack;
 */

namespace FeedBack;

defined('ABSPATH') || exit;

/**
 * Class Feedback_List_Block - registers custom gutenberg block.
 */
class Feedback_List_Block
{

	public static $posts_per_page = 10;
	/**
	 * Init block's logic.
	 */
	public static function init()
	{
		/**
		 * Registering feedback block
		 */
		add_action('init', [__CLASS__, 'gutenberg_add_feedback_list_block']);

		/**
		 * Ajax actions for block data submitting
		 */
		add_action('wp_ajax_feedback_list_details_ajax', [__CLASS__, 'feedback_list_details_ajax']);

		/**
		 * Ajax actions for block data submitting
		 */
		add_action('wp_ajax_feedback_list_load_more', [__CLASS__, 'feedback_list_load_more']);
	}

	/**
	 * Rendering feedback form block in front and in Editor
	 *
	 * @param array  $attributes
	 * @param string $content
	 * @return void
	 */
	public static function feedback_rendered_block($attributes, $content)
	{
		if (!current_user_can('administrator')) {
			return;
		}

		$paged = 1;

		$query = new \WP_Query(
			[
				'post_type' => 'feedback',
				'posts_per_page' => self::$posts_per_page,
				'paged' => $paged,
			]
		);

		// Start capture.
		ob_start();

		echo '<div class="feedback-list-wrapper">';

		while ($query->have_posts()) {
			$query->the_post();
			require FEEDBACK_PLUGIN_DIR_PATH . 'templates/feedback-list-template.php';
		}

		echo '</div>';

		// wp nonce for security
		wp_nonce_field('feedback_list_details', 'feedback_nonce');

		/**
		 * If max num pages more than one show load more button
		 */
		if ($query->max_num_pages > 1) {
			printf('<div class="feedback-load-more" data-paged="2">%s<div class="loader"></div></div>', __('Load More', 'feedback-plugin'));
		}
		// Return capture.
		return ob_get_clean();
	}

	/**
	 * Registering block
	 *
	 * @return void
	 */
	public static function gutenberg_add_feedback_list_block()
	{

		if (!function_exists('register_block_type')) {
			// Gutenberg is not active.
			return;
		}

		$block_args = array(
			'editor_script'   => 'gutenberg-feedback-block-editor',
			'editor_style'    => 'gutenberg-feedback-block-editor',
			'style' => 'gutenberg-feedback-block-front',
			'render_callback' => array(__CLASS__, 'feedback_rendered_block'),
		);

		register_block_type(
			'gutenberg-block/feedback-list-block',
			$block_args
		);
	}

	/**
	 * When form submitted ajax handler
	 *
	 * @return void
	 */
	public static function feedback_list_details_ajax()
	{
		if (empty($_POST['post_ID']))
			return;

		if (!wp_verify_nonce($_POST['feedback_nonce'], 'feedback_list_details'))
			wp_send_json_error(__('Security issue please update the page and try again.', 'feedback-plugin'));

		$post_ID = $_POST['post_ID'];

		// Start capture.
		ob_start();

		require FEEDBACK_PLUGIN_DIR_PATH . 'templates/feedback-list-details.php';

		wp_send_json_success(ob_get_clean());
	}

	/**
	 * Load more button 
	 */
	public static function feedback_list_load_more()
	{
		if (empty($_POST['paged']))
			return;

		if (!wp_verify_nonce($_POST['feedback_nonce'], 'feedback_list_details'))
			wp_send_json_error(__('Security issue please update the page and try again.', 'feedback-plugin'));

		$paged = $_POST['paged'];

		$query = new \WP_Query(
			[
				'post_type' => 'feedback',
				'posts_per_page' => self::$posts_per_page,
				'paged' => $paged,
			]
		);

		// Start capture.
		ob_start();

		while ($query->have_posts()) {
			$query->the_post();
			require FEEDBACK_PLUGIN_DIR_PATH . 'templates/feedback-list-template.php';
		}

		wp_send_json_success([
			'list_elements' => ob_get_clean(),
			'max_num_pages' => $query->max_num_pages
		]);
	}
}

Feedback_List_Block::init();
