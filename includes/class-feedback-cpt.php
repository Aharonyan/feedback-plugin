<?php

/**
 * Gutenberg block to display Feedback.
 *
 * @package FeedBack;
 */

namespace FeedBack;

defined('ABSPATH') || exit;

/**
 * Class Feedback - registers custom gutenberg block.
 */
class Feedback_CPT
{
    /**
     * Init logic.
     */
    public static function init()
    {
        /**
         * Registering custom post type
         */
        add_action('init', [__CLASS__, 'register_post_types']);

        /**
         * Adding metabox with feedback fields
         */
        add_action('add_meta_boxes', [__CLASS__, 'feedback_add_custom_box']);

        /**
         * Adding scripts to custom post type
         */
        add_action('admin_enqueue_scripts', [__CLASS__, 'add_admin_scripts'], 10, 1);

        /**
         * When saving save fields
         */
        add_action('save_post_feedback', [__CLASS__, 'save_feedback_data'], 10, 3);
    }

    /**
     * Adding custom metabox
     *
     * @return void
     */
    public static function feedback_add_custom_box()
    {
        $screens = ['feedback'];
        add_meta_box('feedback_metabox', 'Feedback Data', [__CLASS__, 'feedback_meta_box_callback'], $screens);
    }

    /**
     * Metabox showing function
     *
     * @param [type] $post
     * @param [type] $meta
     * @return void
     */
    public static function feedback_meta_box_callback($post, $meta)
    {
        $screens = $meta['args'];

        // wp nonce for security
        wp_nonce_field('feedback_nonce_cpt', 'feedback_nonce_cpt');

        $first_name = get_post_meta($post->ID, 'feedback_first_name', 1);
        $last_name = get_post_meta($post->ID, 'feedback_last_name', 1);
        $email = get_post_meta($post->ID, 'feedback_email', 1);
        $subject = get_post_meta($post->ID, 'feedback_subject', 1);
        $message = get_post_meta($post->ID, 'feedback_message', 1);

        /**
         * Form template for CPT
         */
        require FEEDBACK_PLUGIN_DIR_PATH . 'templates/feedback-form-template-cpt.php';
    }

    public static function save_feedback_data($post_ID, $post, $update)
    {
        /**
         * If ajax working
         */
        if (wp_doing_ajax())
            return;

        /**
         * Check wp nonce
         */
        if (!wp_verify_nonce($_POST['feedback_nonce_cpt'], 'feedback_nonce_cpt'))
            return;

        /**
         * If this is auto save do nothing
         */
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return;

        /**
         * Check user roles
         */
        if (!current_user_can('edit_post', $post_ID))
            return;

        /**
         * Saving data
         */
        update_post_meta($post_ID, 'feedback_first_name', sanitize_text_field($_POST['feedback_first_name']));
        update_post_meta($post_ID, 'feedback_last_name', sanitize_text_field($_POST['feedback_last_name']));
        update_post_meta($post_ID, 'feedback_email', sanitize_email($_POST['feedback_email']));
        update_post_meta($post_ID, 'feedback_subject', sanitize_text_field($_POST['feedback_subject']));
        update_post_meta($post_ID, 'feedback_message', sanitize_text_field($_POST['feedback_message']));
    }

    /**
     * Adding scripts to custom post type
     *
     * @param [type] $hook
     * @return void
     */
    public static function add_admin_scripts($hook)
    {

        global $post;

        if ($hook == 'post-new.php' || $hook == 'post.php') {
            if ('feedback' === $post->post_type) {
                wp_enqueue_style('CPT-feedback-block-front', FEEDBACK_PLUGIN_URL . '/assets/editor/main.css');
            }
        }
    }

    /**
     * Registering post type
     *
     * @return void
     */
    public static function register_post_types()
    {
        register_post_type('feedback', [
            'label'  => null,
            'labels' => [
                'name'               => __('Feedback', 'feedback-plugin'),
                'singular_name'      => __('Feedback', 'feedback-plugin'),
                'add_new'            => __('Add Feedback', 'feedback-plugin'),
                'add_new_item'       => __('Add Feedback', 'feedback-plugin'),
                'edit_item'          => __('Edit Feedback', 'feedback-plugin'),
                'new_item'           => __('New Feedback', 'feedback-plugin'),
                'view_item'          => __('Watch Feedback', 'feedback-plugin'),
                'search_items'       => __('Search Feedback', 'feedback-plugin'),
                'not_found'          => __('Not Found', 'feedback-plugin'),
                'not_found_in_trash' => __('Not found in trash', 'feedback-plugin'),
                'parent_item_colon'  => '',
                'menu_name'          => __('Feedbacks', 'feedback-plugin'),
            ],
            'description'         => '',
            'public'              => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'show_in_rest'        => false,
            'rest_base'           => null,
            'menu_position'       => 10,
            'exclude_from_search' => true,
            'menu_icon'           => 'dashicons-format-quote',
            'capability_type'   => 'post',
            'capabilities'      => array(
                'edit_post'          => 'update_core',
                'read_post'          => 'update_core',
                'delete_post'        => 'update_core',
                'edit_posts'         => 'update_core',
                'edit_others_posts'  => 'update_core',
                'delete_posts'       => 'update_core',
                'publish_posts'      => 'update_core',
                'read_private_posts' => 'update_core'
            ),
            'map_meta_cap'      => null,
            'hierarchical'        => false,
            'supports'            => ['title'],
            'has_archive'         => false,
            'rewrite'             => true,
            'query_var'           => true,
        ]);
    }
}

Feedback_CPT::init();
