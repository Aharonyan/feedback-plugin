<?php

/**
 * Gutenberg block to display Feedback Form.
 *
 * @package FeedBack;
 */

namespace FeedBack;

defined('ABSPATH') || exit;

/**
 * Class FeedBack_Block - registers custom gutenberg block.
 */
class FeedBack_Form_Block
{


	/**
	 * Init block's logic.
	 */
	public static function init()
	{
		/**
		 * Registering feedback block
		 */
		add_action('init', [__CLASS__, 'gutenberg_add_feedback_block']);

		/**
		 * Ajax actions for block data submitting
		 */
		add_action('wp_ajax_feedback_form_submit', [__CLASS__, 'feedback_form_submit_ajax']);
		add_action('wp_ajax_nopriv_feedback_form_submit', [__CLASS__, 'feedback_form_submit_ajax']);
	}

	/**
	 * Register scripts for frontend
	 *
	 * @return void
	 */
	public static function gutenberg_add_feedback_block_front_scripts()
	{

		$asset = require FEEDBACK_PLUGIN_DIR_PATH . 'assets/frontend/frontend.asset.php';

		wp_register_style(
			'gutenberg-feedback-block-front',
			FEEDBACK_PLUGIN_URL . '/assets/frontend/main.css',
			array(),
			$asset['version']
		);

		wp_register_script(
			'feedback_form',
			FEEDBACK_PLUGIN_URL . '/assets/frontend/frontend.js',
			$asset['dependencies'],
			$asset['version'],
			true
		);

		$data = [
			'ajax_url' => admin_url('admin-ajax.php')
		];

		wp_localize_script('feedback_form', 'feedback_form_data', $data);

		wp_enqueue_script('feedback_form');
	}

	/**
	 * Rendering feedback form block in front and in Editor
	 *
	 * @param array  $attributes
	 * @param string $content
	 * @return void
	 */
	public static function feedback_rendered_block($attributes, $content)
	{
		// Start capture.
		ob_start();

		require FEEDBACK_PLUGIN_DIR_PATH . 'templates/feedback-form-template.php';

		// Return capture.
		return ob_get_clean();
	}

	/**
	 * Registering block
	 *
	 * @return void
	 */
	public static function gutenberg_add_feedback_block()
	{

		if (!function_exists('register_block_type')) {
			// Gutenberg is not active.
			return;
		}

		$block_args = array(
			'editor_script'   => 'gutenberg-feedback-block-editor',
			'editor_style'    => 'gutenberg-feedback-block-editor',
			'style' => 'gutenberg-feedback-block-front',
			'render_callback' => array(__CLASS__, 'feedback_rendered_block'),
		);

		register_block_type(
			'gutenberg-block/feedback-block',
			$block_args
		);
	}

	/**
	 * When form submitted ajax handler
	 *
	 * @return void
	 */
	public static function feedback_form_submit_ajax()
	{
		if (empty($_POST['form_fields']))
			return;

		parse_str($_POST['form_fields'], $form_data);

		if (!wp_verify_nonce($form_data['feedback_nonce'], 'feedback_form_submit'))
			wp_send_json_error(__('Security issue please update the page and try again.', 'feedback-plugin'));

		$post_data = array(
			'post_title'    => wp_strip_all_tags($form_data['subject']),
			'post_status'   => 'publish',
			'post_type'     => 'feedback',
			'post_author'   => 1,
		);

		$post_ID = wp_insert_post($post_data);

		/**
		 * Saving data
		 */
		update_post_meta($post_ID, 'feedback_first_name', sanitize_text_field($form_data['first_name']));
		update_post_meta($post_ID, 'feedback_last_name', sanitize_text_field($form_data['last_name']));
		update_post_meta($post_ID, 'feedback_email', sanitize_email($form_data['email']));
		update_post_meta($post_ID, 'feedback_subject', sanitize_text_field($form_data['subject']));
		update_post_meta($post_ID, 'feedback_message', sanitize_text_field($form_data['message']));

		wp_send_json_success(__('Thank you for sending us your feedback', 'feedback_form_submit'));
	}
}

FeedBack_Form_Block::init();
