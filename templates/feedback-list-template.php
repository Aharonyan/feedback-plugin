<?php
$post_ID = get_the_ID();
$first_name = get_post_meta($post_ID,'feedback_first_name',1);
$last_name = get_post_meta($post_ID,'feedback_last_name',1);
$email = get_post_meta($post_ID,'feedback_email',1);
$published_date = get_the_date();

?>
<div class="feedback_single_list_item_wrap" data-postID="<?= $post_ID ?>">
    <span class="feedback-subject"><?php the_title(); ?></span>
    <span class="feedback-published_date"><?= $published_date; ?></span>
</div>
<div class="feedback_single_details"></div>

