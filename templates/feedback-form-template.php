<?php
$input_text = '<input type="%s" name="%s" id="%s" placeholder="%s" value="%s" required>';
$first_name = null;
$last_name = null;
$email = null;
$user = wp_get_current_user();

if ($user->exists()) {
    $first_name = $user->first_name;
    $last_name = $user->last_name;
    $email = $user->user_email;
}

?>
<div class="feedback-form-wrapper">
    <p class="feedback-title"><?php echo __('Submit your feedback', 'feedback-plugin'); ?></p>
    <form class="feedback-form">
        <div class="form-wrapper">
            <?php printf($input_text, 'text', 'first_name', 'first_name', __('First Name', 'feedback-plugin'), $first_name); ?>
            <?php printf($input_text, 'text', 'last_name', 'last_name', __('Last Name', 'feedback-plugin'), $last_name); ?>
        </div>
        <div class="form-wrapper">
            <?php printf($input_text, 'email', 'email', 'email', __('Email', 'feedback-plugin'), $email); ?>
            <?php printf($input_text, 'text', 'subject', 'subject', __('Subject', 'feedback-plugin'), null); ?>
        </div>
        <?php
        // wp nonce for security
        wp_nonce_field('feedback_form_submit', 'feedback_nonce');
        ?>
        <textarea name="message" id="message" rows="6" placeholder="<?php echo __('Message', 'feedback-plugin'); ?>" required></textarea>
        <div class="submit_button_wrap">
            <input type="submit" value="<?php echo __('Send', 'feedback-plugin'); ?>">
            <div class="loader"></div>
        </div>
        
    </form>
    <div class="response-message"></div>
</div>