<?php
$input_text = '<input type="%s" size="25" name="%s" id="%s" placeholder="%s" value="%s" required>';
$input_textarea = '<textarea name="%s" id="%s" size="40" rows="6" placeholder="%s" required>%s</textarea>';

$first_name = get_post_meta($post->ID, 'feedback_first_name', 1);
$last_name = get_post_meta($post->ID, 'feedback_last_name', 1);
$email = get_post_meta($post->ID, 'feedback_email', 1);
$subject = get_post_meta($post->ID, 'feedback_subject', 1);
$message = get_post_meta($post->ID, 'feedback_message', 1);

?>
<div class="feedback-form">
    <div class="form-wrapper">
        <?php printf($input_text, 'text', 'feedback_first_name', 'first_name', __('First Name', 'feedback-plugin'), $first_name); ?>
        <?php printf($input_text, 'text', 'feedback_last_name', 'last_name', __('Last Name', 'feedback-plugin'), $last_name); ?>
    </div>
    <div class="form-wrapper">
        <?php printf($input_text, 'email', 'feedback_email', 'email', __('Email', 'feedback-plugin'), $email); ?>
        <?php printf($input_text, 'text', 'feedback_subject', 'subject', __('Subject', 'feedback-plugin'), $subject); ?>

    </div>
    <?php printf($input_textarea, 'feedback_message', 'message', __('Message', 'feedback-plugin'), $message); ?>

</div>