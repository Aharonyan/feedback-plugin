<?php
$first_name = get_post_meta($post_ID, 'feedback_first_name', 1);
$last_name = get_post_meta($post_ID, 'feedback_last_name', 1);
$email = get_post_meta($post_ID, 'feedback_email', 1);
$message = get_post_meta($post_ID, 'feedback_message', 1);
$published_date = get_the_date();

?>

<?php printf('<p><strong>%s:</strong> %s</p>', __('First Name', 'feedback-plugin'), $first_name) ?>
<?php printf('<p><strong>%s:</strong> %s</p>', __('Last Name', 'feedback-plugin'), $last_name) ?>
<?php printf('<p><strong>%s:</strong> %s</p>', __('Email', 'feedback-plugin'), $email) ?>
<?php printf('<p><strong>%s:</strong><br>%s</p>', __('Message', 'feedback-plugin'), $message) ?>