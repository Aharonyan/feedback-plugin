/**
 * Adding the block to the editor
 */
((
    compose,
    blocks,
    components,
    i18n,
    element,
    serverSideRender
) => {

    var el = element.createElement,
        __ = i18n.__,
        withState = compose.withState,
        ServerSideRender = serverSideRender;


    blocks.registerBlockType('gutenberg-block/feedback-block', {
        title: __('Feedback Block', 'gutenberg-block'),
        icon: 'format-quote',
        category: 'layout',
        attributes: {},
        edit: function (props) {
            return (
                <ServerSideRender
                    block="gutenberg-block/feedback-block"
                    attributes={props.attributes}
                />
            );
        }
    });
})(
    window.wp.compose,
    window.wp.blocks,
    window.wp.components,
    window.wp.i18n,
    window.wp.element,
    window.wp.serverSideRender,
);

/**
 * Disabling form
 */
jQuery(document).on('submit', 'form.feedback-form', (event) => {
    event.preventDefault();
})

