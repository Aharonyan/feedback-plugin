/**
 * Feedback form submitting logic
 */
(($) => {

    $(document).on('click', '.feedback-list-wrapper .feedback_single_list_item_wrap', (event) => {

        let wrapper = $(event.target),
            postID = wrapper.attr('data-postID'),
            paged = wrapper.attr('data-paged'),
            details = wrapper.next();

        /**
         * If the details already exist
         */
        if (!details.is(':empty')) {
            return;
        }

        const formData = new FormData();
        formData.append('action', 'feedback_list_details_ajax');
        formData.append('post_ID', postID);
        formData.append('paged', paged);
        formData.append('feedback_nonce', $('#feedback_nonce').val());

        fetch(feedback_form_data.ajax_url, {
            method: 'POST',
            body: formData
        })
            .then(response => response.json())
            .then(response => {
                if (response.success) {
                    details.append(response.data);
                    $('.feedback_single_details').fadeOut();
                    details.fadeIn();
                } else {
                    details.append(response.data);
                    $('.feedback_single_details').fadeOut();
                    details.fadeIn();
                }
            }).catch()
    })

    /**
     * Load more list
     */
    $(document).on('click', '.feedback-load-more', (event) => {

        let list_wrapper = $('.feedback-list-wrapper'),
            button = $(event.target),
            paged = button.attr('data-paged');

            button.addClass('waiting');

        const formData = new FormData();
        formData.append('action', 'feedback_list_load_more');
        formData.append('paged', paged);
        formData.append('feedback_nonce', $('#feedback_nonce').val());

        fetch(feedback_form_data.ajax_url, {
            method: 'POST',
            body: formData
        })
            .then(response => response.json())
            .then(response => {
                if (response.success) {
                    list_wrapper.append(response.data.list_elements)
                    button.removeClass('waiting');
                    if(parseInt(response.data.max_num_pages) > parseInt(paged)){
                        button.attr('data-paged', parseInt(paged) + 1)
                    } else {
                        button.fadeOut();
                    }
                    
                } else {
                    console.log(response.data);                    
                }
            }).catch()
    })

}
)(jQuery)