/**
 * Feedback form submitting logic
 */
(($) => {

    $(document).on('submit', 'form.feedback-form', (event) => {
        event.preventDefault();

        const formData = new FormData();
        let form = $('form.feedback-form'),
            submit_button = form.find('input[type=submit]'),
            message = form.parent().find('.response-message');

        formData.append('action', 'feedback_form_submit');
        formData.append('form_fields', $(event.target).serialize());

        fetch(feedback_form_data.ajax_url, {
            method: 'POST',
            body: formData
        })
            .then(response => response.json())
            .then(response => {
                console.log(response);
                if (response.success) {
                    message.append(response.data);
                    form.fadeOut();
                    message.fadeIn();
                } else {
                    message.append(response.data);
                    form.fadeOut();
                    message.fadeIn();
                }
            }).catch()
    })

}
)(jQuery)