/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/frontend.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/blocks/feedback-form-block-front.js":
/*!****************************************************!*\
  !*** ./src/js/blocks/feedback-form-block-front.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Feedback form submitting logic
 */
(function ($) {
  $(document).on('submit', 'form.feedback-form', function (event) {
    event.preventDefault();
    var formData = new FormData();
    var form = $('form.feedback-form'),
        submit_button = form.find('input[type=submit]'),
        message = form.parent().find('.response-message');
    formData.append('action', 'feedback_form_submit');
    formData.append('form_fields', $(event.target).serialize());
    fetch(feedback_form_data.ajax_url, {
      method: 'POST',
      body: formData
    }).then(function (response) {
      return response.json();
    }).then(function (response) {
      console.log(response);

      if (response.success) {
        message.append(response.data);
        form.fadeOut();
        message.fadeIn();
      } else {
        message.append(response.data);
        form.fadeOut();
        message.fadeIn();
      }
    }).catch();
  });
})(jQuery);

/***/ }),

/***/ "./src/js/blocks/feedback-list-block-front.js":
/*!****************************************************!*\
  !*** ./src/js/blocks/feedback-list-block-front.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Feedback form submitting logic
 */
(function ($) {
  $(document).on('click', '.feedback-list-wrapper .feedback_single_list_item_wrap', function (event) {
    var wrapper = $(event.target),
        postID = wrapper.attr('data-postID'),
        paged = wrapper.attr('data-paged'),
        details = wrapper.next();
    /**
     * If the details already exist
     */

    if (!details.is(':empty')) {
      return;
    }

    var formData = new FormData();
    formData.append('action', 'feedback_list_details_ajax');
    formData.append('post_ID', postID);
    formData.append('paged', paged);
    formData.append('feedback_nonce', $('#feedback_nonce').val());
    fetch(feedback_form_data.ajax_url, {
      method: 'POST',
      body: formData
    }).then(function (response) {
      return response.json();
    }).then(function (response) {
      if (response.success) {
        details.append(response.data);
        $('.feedback_single_details').fadeOut();
        details.fadeIn();
      } else {
        details.append(response.data);
        $('.feedback_single_details').fadeOut();
        details.fadeIn();
      }
    }).catch();
  });
  /**
   * Load more list
   */

  $(document).on('click', '.feedback-load-more', function (event) {
    var list_wrapper = $('.feedback-list-wrapper'),
        button = $(event.target),
        paged = button.attr('data-paged');
    button.addClass('waiting');
    var formData = new FormData();
    formData.append('action', 'feedback_list_load_more');
    formData.append('paged', paged);
    formData.append('feedback_nonce', $('#feedback_nonce').val());
    fetch(feedback_form_data.ajax_url, {
      method: 'POST',
      body: formData
    }).then(function (response) {
      return response.json();
    }).then(function (response) {
      if (response.success) {
        list_wrapper.append(response.data.list_elements);
        button.removeClass('waiting');

        if (parseInt(response.data.max_num_pages) > parseInt(paged)) {
          button.attr('data-paged', parseInt(paged) + 1);
        } else {
          button.fadeOut();
        }
      } else {
        console.log(response.data);
      }
    }).catch();
  });
})(jQuery);

/***/ }),

/***/ "./src/js/frontend.js":
/*!****************************!*\
  !*** ./src/js/frontend.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _blocks_feedback_form_block_front_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./blocks/feedback-form-block-front.js */ "./src/js/blocks/feedback-form-block-front.js");
/* harmony import */ var _blocks_feedback_form_block_front_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_blocks_feedback_form_block_front_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _blocks_feedback_list_block_front_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./blocks/feedback-list-block-front.js */ "./src/js/blocks/feedback-list-block-front.js");
/* harmony import */ var _blocks_feedback_list_block_front_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_blocks_feedback_list_block_front_js__WEBPACK_IMPORTED_MODULE_1__);
/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */



/***/ })

/******/ });
//# sourceMappingURL=frontend.js.map