<?php

/**
 * @wordpress-plugin
 * Plugin Name:       Feedback Plugin
 * Description:       Leave your feedback
 * Version:           1.0.0
 * Author:            Aleksan Aharonyan
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       feedback-plugin
 * Domain Path:       /languages
 * PHP requires at least: 5.6
 * WP requires at least: 5.0
 * Tested up to: 5.6
 */

namespace FeedBack;

// If this file is called directly, abort.
defined('ABSPATH') || exit;


/**
 * Core
 */
class FeedBack
{

	/**
	 * Initialization of the plugin
	 */
	public static function init()
	{

		define('FEEDBACK_PLUGIN_URL', plugins_url('', __FILE__));
		define('FEEDBACK_PLUGIN_DIR_PATH', plugin_dir_path(__FILE__));
		define('FEEDBACK_PLUGIN_Template_PATH', plugin_dir_path(__FILE__) . 'templates/');

		/**
		 * Currently plugin version.
		 * Start at version 1.0.0 and use SemVer - https://semver.org
		 * Rename this for your plugin and update it as you release new versions.
		 */
		define('PLUGIN_NAME_VERSION', '1.0.0');

		register_activation_hook(__FILE__, [__CLASS__, 'fe_plugin_activate']);

		register_deactivation_hook(__FILE__, [__CLASS__, 'fe_plugin_deactivate']);

		add_action('plugins_loaded', [__CLASS__, 'true_load_plugin_textdomain']);

		add_action('plugins_loaded', [__CLASS__, 'add_components']);
	}

	/**
	 * On plugin activate
	 *
	 * @return void
	 */
	public static function fe_plugin_activate()
	{
		do_action('feedback_plugin_activate');
	}

	/**
	 * On plugin deactivate
	 *
	 * @return void
	 */
	public static function fe_plugin_deactivate()
	{
		do_action('feedback_plugin_deactivate');
	}

	/**
	 * Add Components
	 */
	public static function add_components()
	{

		/**
		 * Adding Feedback CPT
		 */
		require_once __DIR__ . '/includes/class-feedback-cpt.php';

		/**
		 * Adding Feedback Gutenberg block logic
		 */
		require_once __DIR__ . '/includes/class-feedback-form-block.php';

		/**
		 * Adding Feedback Gutenberg block logic
		 */
		require_once __DIR__ . '/includes/class-feedback-list-block.php';

		/**
		 * Registering block front scripts
		 */
		add_action('wp_enqueue_scripts', [__CLASS__, 'gutenberg_add_feedback_block_front_scripts']);

		/**
		 * Registering block scripts in gutenberg editor
		 */
		add_action('enqueue_block_editor_assets', [__CLASS__, 'gutenberg_feedback_block_editor_scripts']);
		
	}


	/**
	 * Add languages
	 *
	 * @return void
	 */
	public static function true_load_plugin_textdomain()
	{
		load_plugin_textdomain('front-editor', false, dirname(plugin_basename(__FILE__)) . '/languages/');
	}

	/**
	 * Adding script to gutenberg
	 *
	 * @return void
	 */
	public static function gutenberg_feedback_block_editor_scripts()
	{

		$asset = require FEEDBACK_PLUGIN_DIR_PATH . 'assets/editor/editor.asset.php';

		wp_register_script(
			'gutenberg-feedback-block-editor',
			FEEDBACK_PLUGIN_URL . '/assets/editor/editor.js',
			$asset['dependencies'],
			$asset['version'],
			true
		);

		wp_register_style(
			'gutenberg-feedback-block-editor',
			FEEDBACK_PLUGIN_URL . '/assets/editor/main.css',
			array(),
			$asset['version']
		);

		wp_enqueue_script('gutenberg-feedback-block-editor');
	}

	/**
	 * Register scripts for frontend
	 *
	 * @return void
	 */
	public static function gutenberg_add_feedback_block_front_scripts()
	{

		$asset = require FEEDBACK_PLUGIN_DIR_PATH . 'assets/frontend/frontend.asset.php';

		wp_register_style(
			'gutenberg-feedback-block-front',
			FEEDBACK_PLUGIN_URL . '/assets/frontend/main.css',
			array(),
			$asset['version']
		);

		wp_register_script(
			'feedback_form',
			FEEDBACK_PLUGIN_URL . '/assets/frontend/frontend.js',
			$asset['dependencies'],
			$asset['version'],
			true
		);

		$data = [
			'ajax_url' => admin_url('admin-ajax.php')
		];

		wp_localize_script('feedback_form', 'feedback_form_data', $data);

		wp_enqueue_script('feedback_form');
	}

	
}

FeedBack::init();
